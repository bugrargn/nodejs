//  Http Modülü
var http = require('http');

// sunucu nesnesi oluştur
http.createServer(function (req, res) {
    //res.writeHead(200, {'Content-Type': 'text/html'}); // HTTP Header - Gelen Yanıt HTML gerekliyse
    res.write('Merhaba Dünya'); // istemciye yanıt yaz
    // res.write(req.url); //
    res.end(); // yanıtı bitir
}).listen(8080); // sunucu nesnesi 8080 portunda dinlesin.