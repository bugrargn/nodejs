var url = require('url');
var adr = 'http://localhost:8080/default.htm?year=2021&month=ocak';
// url.parse ile ayrıştırma olursa adresin her bir parçasını özellik olarak içeren URL nesnesi döndürür.
var q = url.parse(adr, true);

console.log(q.host); // localhost:8080
console.log(q.pathname); // '/default.htm'
console.log(q.search); // ?year=2021&month=ocak

var qdata = q.query; // nesne döndürür: {year:2021, month:'ocak'}
console.log(qdata.month); // 'şubat'