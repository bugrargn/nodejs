var http = require('http');
var uc = require('upper-case'); // Büyük harf paketi

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(uc.upperCase("Merhaba Dünya!"));
    res.end();
}).listen(8080);