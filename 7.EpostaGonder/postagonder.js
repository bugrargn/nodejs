// npm install nodemailer
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'postaniz@gmail.com',
        pass: 'şifreniz'
    }
});

var mailOptions = {
    from: 'posta@gmail.com',
    to: 'alici@posta.com',
    subject: 'Node.js ile eposta gönderimi',
    text: 'Node.js eposta gönderiyor!'
    // html: '<h1>Merhaba</h1><p>Mesaj</p>'
};
transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
        console.log(error);
    } else {
        console.log('Posta gönderildi: ' + info.response);
    }
});