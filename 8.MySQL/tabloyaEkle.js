var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "kullanıcıadi",
    password: "şifre",
    database: "vt"
});

// Müşteri tablo
con.connect(function(err) {
    if (err) throw err;
    console.log("Bağlandı");
    var sql = "INSERT INTO musteri (isim, adres) VALUES ('Şirket AŞ.', 'Türkiye')";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("1 kayıt eklendi.");
    });
});