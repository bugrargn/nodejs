var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "kullanıcıadi",
    password: "şifre",
    database: "vt"
});

// Müşteri tablo
// Tabloda Primary Key varsa Otomatik artan ID'yi al insertId

// {
//   fieldCount: 0,
//   affectedRows: 14,
//   insertId: 0,
//   serverStatus: 2,
//   warningCount: 0,
//   message: '\'Records:14  Duplicated: 0  Warnings: 0',
//   protocol41: true,
//   changedRows: 0
// }

con.connect(function(err) {
    if (err) throw err;
    console.log("Bağlandı");
    var sql = "INSERT INTO musteri (isim, adres) VALUES ('Ahmet', 'Aksaray')";
    
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("1 kayıt eklendi, ID " + result.insertId);
    });
});