var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "kullanıcıadi",
    password: "şifre",
    database: "vt"
});

// Müşteri tablo
con.connect(function(err) {
    if (err) throw err;
    console.log("Bağlandı");
    var sql = "INSERT INTO musteri (isim, adres) VALUES ?";
    var values = [
        ['Ahmet', 'Aksaray'],
        ['Mehmet', 'Ankara'],
        ['Ayşe', 'İstanbul']
    ];
    con.query(sql, [values], function (err, result) {
        if (err) throw err;
        console.log("Eklenen kayıt sayısı: " + result.affectedRows);
    });
});