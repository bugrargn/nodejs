var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "kullaniciadi",
    password: "şifre"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Bağlandı!");
    con.query("CREATE DATABASE vt", function (err, result) {
        if (err) throw err;
        console.log("Veritabanı oluşturuldu");
    });
});