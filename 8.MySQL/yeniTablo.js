var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "kullanıcıadi",
    password: "şifre",
    database: "vt"
});

// Müşteri tablo
con.connect(function(err) {
    if (err) throw err;
    console.log("Bağlandı");
    //var sql = "CREATE TABLE musteri (id INT AUTO_INCREMENT PRIMARY KEY, isim VARCHAR(255), adres VARCHAR(255))";
    // Tablo zaten varsa ALTER TABLE ile ekleme yapın.
    var sql = "ALTER TABLE musteri ADD COLUMN id INT AUTO_INCREMENT PRIMARY KEY";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Tablo oluşturuldu.");
    });
});